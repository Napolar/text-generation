# AI Framework - Text generation

## Introduction
Nous présentons ici plusieurs configurations de modèle et d'encodage dans le but de faire de la génération de texte. Chacun des modèles est accompagné de _scripts permettant d’entraîner le modèle_. De plus, comme les temps d’entraînements sont relativement longs, certains _scripts sont proposés afin de partir d'un modèle pré-entraîné_.
Les modèles proposés sont les suivants :  

| Modèle | Description |
| :-| :-|
| **char-LSTM**     | Les données sont encodées caractère par caractère et entrainées avec un modèle basé sur une architecture LSTM.  |
| **char-GRU**      | Les données sont encodées caractère par caractère et entrainées avec un modèle basé sur une architecture GRU.  |
| **word2vec-LSTM** | Les données sont encodées en utilisant un modèle Word2Vec entrainé sur le dataset, puis entrainé avec un modèle basé sur une architecture LSTM.  |
| **gpt2**          | Génére du texte à partir du modèle pre-entrainé de gpt2 affiné sur nos données.  |


## Scripts
Afin de faciliter la génération de texte pour chacun des modèles, nous allons détailler les différents scripts disponibles ainsi que leur fonctionnement.

### Requirements

**Concernant la version de python, les différents modèles ont été testés en version `python 3.7`.**

Pour chacun des scripts ci-dessus, il est nécessaire d'initialiser une environnement virtuel (`venv`) dans le dossier, les librairies à télécharger sont détaillées dans les fichiers `requirements.txt` des dossiers respectifs. En effet, les modèles pouvant nécessiter des librairies différentes (nottament GPT2 qui nécessite une version `1.15` de tensorflow).

### char-LSTM & char-GRU
Les deux modèles basés sur l'encodage de caractères sont stockés dans le fichier `simple_char_encoding`. Ils offrent 4 scripts, deux pour entraîner, et deux pour la génération de texte à partir d'un modèle pré-entraîné.

#### LSTM
La majorité des Recurrent Neural Networks (RNN) ne sont pas capables de retenir de l’information sur de très grandes périodes de temps (Long-term dependencies problem).
Les Long Short-Term Memory (LSTM) sont des RNN capables de résoudre ce problème.
Ils contiennent une couche "forget gate layer" qui décide quelles informations supprimer du cell state, ainsi qu'une couche "input gate layer" qui décide des valeurs à mettre à jour et des nouveaux candidats pour ces valeurs.

#### GRU
Les Gated Recurrent Unit (GRU) sont des variantes des LSTM qui combinent les couches "forget gate layer" et "input gate layer".

#### Execution avec virtualenv
_Installation des librairies_
```bash
# create and activate virtualenv
python3 -m virtualenv venv
source venv/bin/activate

# install the needed libraries
python -m pip install -r requirements.txt
```

_Entrainement des modèles_
``` python
# train with lstm based architecture
python train_lstm.py

# train with gru based architecture
python train_gru.py
```

_Génération des textes_
``` python
# load pre-trained weights and generate text using 
# lstm based architecture
python run_lstm.py -d [0.01, 0.05]  -l 200 -n 10

# load pre-trained weights and generate text using 
# grubased architecture
python run_gru.py -d [0.01, 0.05]  -l 200 -n 10
```

* Avec `-d` ou `--diversity` le taux de diversité appliqué à la prédiction.
* Avec `-l` ou `--pred_len` le nombre de lettres pour une prédiction.
* Avec `-n` ou `--pred_number` le nombre de prédictions à effectuer.

Pour chaque prédiction et pour chaque taux de diversité, le modèle va établir une séquence de départ incluse dans le corpus, et générer la suite de cette séquence.

### Word2Vec LSTM & Word2Vec

#### Word2Vec
Word2Vec sont des réseaux de neurones à deux couches. Ces réseaux sont entraînés pour apprendre les représentations vectorielles des mots composant un texte.
L'entraînement se fait en parcourant le texte fourni et en mettant à jours les poids associés aux neurones, de telle sorte que l'erreur de prédiction soit minimale.
Word2Vec possède deux architectures neuronales :
- Continuous Bag of Words (CBOW) prédit un mot à partir d'un contexte lexical (i.e. des mots voisins)
- Skip-Gram prédit un contexte lexical à partir d'un mot

En entraînant nos réseaux sur un corpus composé de plusieurs poèmes, nous obtenons, en sortie, les encodages de chaque mot du corpus donné en entrée.
Nous n'avons considéré que l'architecture neuronale CBOW. Dans ce cas, les vecteurs d'encodage sont établis en tenant compte du contexte de chaque mot.
Ces vecteurs d'encodage ont ensuité été donnés en entrée des réseaux de type LSTM et GRU.

#### Execution avec virtualenv
_Installation des librairies_
```bash
# create and activate virtualenv
python3 -m virtualenv venv
source venv/bin/activate

# install the needed libraries
python -m pip install -r requirements.txt
```

_Execution de l'entrainement de word2vec et lstm_
``` python
# train with lstm based architecture
python train_w2v_lstm.py
```

### GPT2

GPT2 est un algorithme développé par le laboratoire OpenAI dans le but de générer du texte. Il a récemment été rendu célèbre par la cohérence des résutats générés. Il s'inspire des algoruthmes à base de modules Transformers, et est constitué d'une succession de couches Decoder.
Ce dépot contient deux scripts faisant tourner l'algorithme GPT2 :
- à partir de poids pré-entraînés (small_model : modèle à 12 couches Decoder, big_model : modèle à 24 couches Decoder) que l'on affine à partir de notre corpus de poèmes
- à partir d'une de nos exécutions du script précédent (small_model avec 1500 époques ou big_model avec 4000 époques)

Le second script nécessite le téléchargement d'un de nos dossiers "checkpoint" pour récupérer les poids pré-entraînés et affinés sur le corpus à l'adresse suivante :
https://mega.nz/#F!AgNWyY5S!KOPw5GQAIFet5NAoKaW7mQ
Il vous faudra les glisser dans le répertoire `checkpoint` du dossier `gpt-2`.

#### Execution avec virtualenv
_Installation des librairies_
```bash
# create and activate virtualenv
python3 -m virtualenv venv
source venv/bin/activate

# install the needed libraries
python -m pip install -r requirements.txt
```

_Execution de l'entrainement de gpt-2_
``` python
# Fine tune gpt-2
python train.py --size [small | big]
```

_Execution de la génération de texte de gpt-2_.
Les checkpoints doivent être téléchargés.
``` python
# Generate from pre-trained weights
python run.py --size [small | big]
```
* Avec `-s` ou `--size` le taille du modèle de base de gpt-2.


## Jeu de données
Les données sur lesquelles nous basons nos modèles sont incluses dans le dépôt. Il s'agit d'un corpus de poèmes issus des **"Fleurs du mal"** de Charles Baudelaire. 
Ce jeu de données provient de Gutenberg , qui est une bibliothèque virtuelle constituée essentiellement de livres du domaine public.


## Bibliographie
- **Efficient Estimation of Word Representations in Vector Space** (Tomas Mikolov, Kai Chen, Greg Corrado, Jeffrey Dean, 2013) consultable à l'adresse : https://arxiv.org/abs/1301.3781
- **Long Short-term Memory** (Sepp Hochreiter, Jürgen Schmidhuber, 1997)  consultable à l'adresse : https://www.researchgate.net/publication/13853244_Long_Short-term_Memory
- **Learning Phrase Representations using RNN Encoder-Decoder for Statistical Machine Translation** (Kyunghyun Cho, Bart van Merrienboer, Caglar Gulcehre, Dzmitry Bahdanau, Fethi Bougares, Holger Schwenk, Yoshua Bengio, 2014) consultable à l'adresse : https://arxiv.org/abs/1406.1078
- **Language Models are Unsupervised Multitask Learners** (Alec Radford, Jeffrey Wu, Rewon Child, David Luan, Dario Amodei, Ilya Sutskever, 2019) consultable à l'adresse : https://openai.com/blog/better-language-models/

## Librairies
- https://github.com/RaRe-Technologies/gensim.git (gensim)
- https://github.com/tensorflow/tensorflow.git (Tensorflow)
- https://github.com/openai/gpt-2.git (implémentation initiale de GPT2 par OpenAI)
- https://github.com/minimaxir/gpt-2-simple.git (librairie utilisée pour GPT2)

## Contributeurs
Thomas Fel

Thibault Fourez

Jean-Paul Kaptue

Fanny Mathevet

Fanny Van Rell

Samuel Velmont

_INSA Toulouse, Département Génie Mathématiques et Modélisation_
```
```
