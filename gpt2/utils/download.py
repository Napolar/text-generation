#!/usr/bin/env python
# -*- coding: utf-8 -*-
r""" Utility relative to download """

import progressbar
import requests

def download_file(url, local_file):
    """Download the given file at the given location

    Parameters
    ----------
    url : str
        The url link of the file to download
    local_file : str
        The url of the folder to store the file

    Raises
    ------
    FileNotFound
        If the file is not found
    """
    request_stream = requests.get(url, stream=True)

    if request_stream.status_code != 200:
        raise FileNotFoundError("The file has not been found at url : {}".format(url))

    temp_file      = open(local_file, 'wb')
    file_size      = int(request_stream.headers['Content-Length'])
    
    chunk    = 1
    num_bars = file_size / chunk
    bar      = progressbar.ProgressBar(maxval=num_bars).start()
    
    i = 0
    for chunk in request_stream.iter_content():
        temp_file.write(chunk)
        bar.update(i)
        i+=1
    temp_file.close()
    return