#!/usr/bin/env python
# -*- coding: utf-8 -*-
r""" Rerieve and finetune the GPT-2 Model
     We will use a pre-trained model, and use a
     library called gpt-2-simple to finetune it.

     @Note : as this library use an old version
     of tensorflow, we downgrade to 1.14 or 1.15.
"""

import os
import argparse
import tensorflow as tf
import gpt_2_simple as gpt2

if not tf.__version__[:3] == '1.1':
    raise Exception("Tensorflow version not compatible, please use virtual env and the requirements.")

BAUDELAIRE_DATASET_URL = "../dataset/baudelaire.txt"

parser = argparse.ArgumentParser()
parser.add_argument("-s", "--size",   type=str, help="Type of the model, 'big' or 'small'", default='small')

args = parser.parse_args()

# sanity check
assert os.path.isfile(BAUDELAIRE_DATASET_URL)


"""
As the GPT-2 weights are extensive, we can download
two version, a small one (500 MB after training), 
and a bigger one (1.5GB after training).

Adapted from https://github.com/openai/gpt-2/blob/master/download_model.py
"""

small_model = "124M"
big_model   = "355M"

model_choosen = small_model if args.size == 'small' else big_model
gpt2.download_gpt2(model_name=model_choosen)

# define folder to store the data
checkpoint_dir = "./checkpoints"

# start the fine-tuning
sess = gpt2.start_tf_sess()
gpt2.finetune(sess,
              dataset=BAUDELAIRE_DATASET_URL,
              model_name=model_choosen,
              checkpoint_dir=checkpoint_dir,
              steps=2000,
              run_name="#1",
              print_every=10,
              sample_every=200,
              save_every=500,
              restore_from="latest",
              )

# after fine tuning, generate using the last run
gpt2.generate(sess, run_name="#1", checkpoint_dir=checkpoint_dir)

