#!/usr/bin/env python
# -*- coding: utf-8 -*-
r""" Rerieve an already finetuned GPT-2model
     and generate poems.

     @Note : as this library use an old version
     of tensorflow, we downgrade to 1.14 or 1.15.
"""

import os
import argparse
import tarfile
import tensorflow as tf
import gpt_2_simple as gpt2


from utils.download import download_file

if not tf.__version__[:3] == '1.1':
    raise Exception("Tensorflow version not compatible, please use virtual env and the requirements.")

parser = argparse.ArgumentParser()
parser.add_argument("-s", "--size",   type=str, help="Type of the model, 'big' or 'small'", default='small')

args = parser.parse_args()


"""
As the GPT-2 weights are extensive, we can download
two version, a small one (500 MB after training), 
and a bigger one (1.5GB after training).

Adapted from https://github.com/openai/gpt-2/blob/master/download_model.py
"""

small_model = {
    'url' : "http://drive.com/checkpoint_run_big.tar",
    'run' : 'run_small',
    'name': 'checkpoint_small.tar'
}
big_model = {
    'url' : "http://drive.com/checkpoint_run_big.tar",
    'run' : 'run_2',
    'name': 'checkpoint_big.tar'
}

model = small_model if args.size == 'small' else big_model

# define folder to store the data
checkpoint_dir  = "./checkpoint"
checkpoint_file = os.path.join(checkpoint_dir, model['name'])

# check the existence of the checkpoint
if not os.path.isfile(checkpoint_file):
    raise FileNotFoundError("Checkpoint should be located at {}".format(checkpoint_file))

# untar the checkpoint
with tarfile.open(checkpoint_file) as tar:
    tar.extractall()

# retrieve the run from the checkpoint and generate text
sess = gpt2.start_tf_sess()
gpt2.load_gpt2(sess,
               run_name=model['run'],
               checkpoint_dir=checkpoint_dir,
               model_name=None,
               multi_gpu=False)

gpt2.generate(sess, run_name=model['run'], checkpoint_dir=checkpoint_dir)

