#!/usr/bin/env python
# -*- coding: utf-8 -*-
r""" Utilities relative to the dataset """

import io
import numpy as np

__all__ = ('download_dataset', 'prepare_dataset')

BAUDELAIRE_DATASET_URL = "../dataset/baudelaire.txt"

# to prepare all the possible sequence of text given a fixed size we need to
# define this fixed size, and also the step beetwen two sequences
SEQUENCE_LEN   = 100
SEQUENCE_DELTA = 5

def download_dataset(url = BAUDELAIRE_DATASET_URL):
    """Download the given dataset and return it as a string (lowercase)

    Parameters
    ----------
    url : str
        The url link of the dataset

    Raises
    ------
    FileNotFound
        If the dataset is empty

    Returns
    -------
    dataset : str
        The dataset as lowercase string
    """
    dataset = ""
    with io.open(url, encoding='utf-8') as f:
        dataset = f.read().lower()

    # sanity check
    if len(dataset) < 1:
        raise FileNotFoundError()


    return dataset

def prepare_dataset(dataset, sequence_len = SEQUENCE_LEN,
                     sequence_delta = SEQUENCE_DELTA):
    """Prepare the dataset using char encoding

    We will generate sequence of text limited by a number of char : X. Then the
    Y will be the letter following this sequence.

    Parameters
    ----------
    dataset : str
        The text dataset as string
    sequence_len : int
        The length of the each sequence
    sequence_delta : int
        The number of letter to skip beetwen two sequences

    Returns
    -------
    X : array of array of chars
        All the possible sequence of text to train
    Y : array of chars
        The missing letter for each sequence
    uniques_chars : list of chars
        List of uniques chars contained in the dataset
    char_to_indice : dict
        Dict for mapping char to their indice
        eg : dic['z'] => 26
    indice_to_char : dict
        Dict for mapping indice to the value of the char
        eg : dic['27'] => 'z'
    """
    print("\n\nThe corpus contains {} letters".format(len(dataset)))

    # sort to get all the unique characters used in the corpus
    # the number of chars will be the size of every one hot vector.
    # prepare the mapping indice <=> chars
    # eg : 1 => 'a' ... 26 => 'z' ...
    uniques_chars  = np.unique(list(dataset))
    char_to_indice = dict((c, i) for i, c in enumerate(uniques_chars))
    indice_to_char = dict((i, c) for i, c in enumerate(uniques_chars))

    print("We have {} differents characters".format(len(uniques_chars)))

    # retrieve all the possible sequences, and for each of them
    # the letter after, this will be the letter to predict
    sentences = []
    next_chars = []
    for i in range(0, len(dataset) - sequence_len, sequence_delta):
        sentences.append(dataset[i:i + sequence_len])
        next_chars.append(dataset[i + sequence_len])

    # one hot encode all words, of all the sequences
    # first init to zero all the vectors
    # Y vector will be sparse vector as a sparse_categorical_loss
    # is available
    X = np.zeros((len(sentences), sequence_len, len(uniques_chars)))
    Y = np.zeros((len(sentences)))

    for sentence_index, sentence in enumerate(sentences):
        # for every sentences
        for char_index, char in enumerate(sentence):
            # for every character in the sentence
            # set 1 to the indice of the char
            X[sentence_index, char_index, char_to_indice[char]] = 1
        Y[sentence_index] = char_to_indice[next_chars[sentence_index]]

    # We now have X, Y ready for the training process
    print("We have {} samples for training".format(len(X)))
    print("Data preparation completed. \n\n")

    return X, Y, uniques_chars, char_to_indice, indice_to_char