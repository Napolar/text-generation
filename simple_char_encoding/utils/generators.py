#!/usr/bin/env python
# -*- coding: utf-8 -*-
r""" Collection of callbacks and utilities relative to text generation"""

import sys

import numpy as np
from tensorflow.keras.callbacks import LambdaCallback

__all__ = ('choose_sample', 'generate_writer_callback')

def print_green(str):
    """Print the givne text in green
    Parameters
    ----------
    str : string
        Text to print in green
    """
    sys.stdout.write("\033[92m {}\033[00m" .format(str))
    sys.stdout.flush()

def choose_sample(predictions, diversity = 1.0, epsilon = 1e-15):
  """Given a predictions (probability array) choose an index
    the diversity factor will give more chance to the unpredicted ones,
    but it will make no sense to just divide the entire array by diversity as
    we used softmax in the last layer. So we first 'revert' the softmax effect
    by log our predictions, then divide all the number by diversity, and re-use
    softmax

    Parameters
    ----------
    predictions : array
        The prediction of the model for the next letter
    diversity : float
        Give more chance to the unpredicted letters
    epsilon : float
        To avoid to divide by zero, be carefull as a value to high could cause
        errors or weird behaviour

    Returns
    -------
    char_index : int
        The index wich has be choosen given the multinomial law
  """
  predictions = np.array(predictions).astype('float64')
  predictions = np.log(predictions  + epsilon) / diversity
  predictions = np.exp(predictions)

  sum_exp       = np.sum(predictions)
  probabilities = predictions / ( sum_exp + epsilon )

  # correct the proba to sum to 1 (python floating error)
  sigma = 1.0 - np.sum(probabilities)
  probabilities[np.argmax(probabilities)] += sigma

  # use multinomial to affect to every character a random score based
  # on the predictions of the model
  scores = np.random.multinomial(1, probabilities, 1)

  # return the index who has the better score : the index of the
  # choosen character
  return np.argmax(scores)

def generate_writer_callback(model,
                             dataset,
                             uniques_chars,
                             char_to_indice,
                             indice_to_char,
                             sequence_len,
                             print_every = 10,
                             min_epoch = 20,
                             diversities = [0.01, 0.1],
                             prediction_len = 200,
                             separator = "#"):
    """Generate a callback that will use the current model to generate text at
       following the differents parameters given

    Parameters
    ----------
    model : tensorflow.model
        Tensorflow model used to predict the next sentence
    dataset : string
        The string dataset containing the text
    uniques_chars : list of chars
        List of uniques chars contained in the dataset
    char_to_indice : dict
        Dict for mapping char to their indice
        eg : dic['z'] => 26
    indice_to_char : dict
        Dict for mapping indice to the value of the char
        eg : dic['27'] => 'z'
    sequence_len : int
        The length of the sequence for the model
    diversities : array of float
        The set of diversities value to try at every generation
    prediction_len : int
        The size of the generated text
    separator : string
        The char to separe when priting result
    """
    def callback(epoch_number, _):

        # trigger only when we satisfy the min epoch and periodic epoch
        if epoch_number < min_epoch or epoch_number % print_every != 0:
            return

        print("\n" * 2, separator * 35)
        print(separator * 5, " Generating text for epoch {} ".format(epoch_number), separator * 5, "\n")

        # take random sequence
        # take just after \n as it will make more sense
        random_start_index = np.random.choice(np.argwhere(np.array(dataset) == '\n').flatten() + 1)
        random_sequence    = dataset[random_start_index : random_start_index + sequence_len]

        for diversity in diversities:
            print(separator * 3, " diversity = {} ".format(diversity), separator * 3)
            print(separator * 35)
            generated = "" + random_sequence
            print_green(generated)

            for _ in range(prediction_len):

                # prepare the random sequence to send to the model
                x_random_sequence = np.zeros(( 1, sequence_len, len(uniques_chars) ))
                for char_index, char in enumerate(generated):
                    x_random_sequence[0, char_index, char_to_indice[char] ] = 1

                # make a prediction, retrieve the next index / char
                predictions = model.predict(x_random_sequence, verbose=0)[0]
                next_index  = choose_sample(predictions, diversity)
                next_char   = indice_to_char[next_index]

                generated = generated[1:] + next_char

                sys.stdout.write(next_char)
                sys.stdout.flush()

            print("\n" * 2)

    return LambdaCallback(on_epoch_end=callback)

def generate_text(model,
                  dataset,
                  uniques_chars,
                  char_to_indice,
                  indice_to_char,
                  sequence_len,
                  diversities = [0.01, 0.1],
                  prediction_len = 400,
                  separator = "#"):
    """Generate text using a trained model

    Parameters
    ----------
    dataset : string
        The string dataset containing the text
    uniques_chars : list of chars
        List of uniques chars contained in the dataset
    char_to_indice : dict
        Dict for mapping char to their indice
        eg : dic['z'] => 26
    indice_to_char : dict
        Dict for mapping indice to the value of the char
        eg : dic['27'] => 'z'
    sequence_len : int
        The length of the sequence for the model
    print_every : int
        Trigger the generation every print_every epochs
    min_epoch : int
        Threshold to start triggering callback
    diversities : array of float
        The set of diversities value to try at every generation
    prediction_len : int
        The size of the generated text
    separator : string
        The char to separe when priting result

    Returns
    -------
    gen_callback : tensorflow.keras.LambdaCallback
        The index wich has be choosen given the multinomial law
    """
    print("\n" * 2)

    # take random sequence
    # take just after \n as it will make more sense
    random_start_index = np.random.choice(np.argwhere(np.array(list(dataset)) == '\n').flatten() + 1)
    random_sequence    = dataset[random_start_index : random_start_index + sequence_len]

    for diversity in diversities:
        print('\n'*2)
        print(separator * 3, " diversity = {} ".format(diversity), separator * 3)
        print(separator * 35)
        generated = "" + random_sequence
        print_green(generated)

        for _ in range(prediction_len):

            # prepare the random sequence to send to the model
            x_random_sequence = np.zeros(( 1, sequence_len, len(uniques_chars) ))
            for char_index, char in enumerate(generated):
                x_random_sequence[0, char_index, char_to_indice[char] ] = 1

            # make a prediction, retrieve the next index / char
            predictions = model.predict(x_random_sequence, verbose=0)[0]
            next_index  = choose_sample(predictions, diversity)
            next_char   = indice_to_char[next_index]

            generated = generated[1:] + next_char

            sys.stdout.write(next_char)
            sys.stdout.flush()

        print("\n" * 2)