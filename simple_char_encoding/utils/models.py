#!/usr/bin/env python
# -*- coding: utf-8 -*-
r""" Expose function to generate LSTM and GRU models """

import tensorflow as tf
from tensorflow.keras.callbacks import LambdaCallback
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import LSTM, Input
from tensorflow.keras.optimizers import RMSprop
from tensorflow.keras.callbacks import ModelCheckpoint

__all__ = ('generate_lstm_model', 'generate_gru_model')

def generate_lstm_model(input_shape, internal_size = 1024, nb_layers = 1):
    """Build a tensorflow lstm model

    Parameters
    ----------
    input_shape : tuple
        The shape of the input vector
    internal_size : int
        The number of units in the LSTM layers
    nb_layers : int
        The number of LSTM Layers

    Returns
    -------
    model : tensorflow.model
    """
    # clear all previous session
    tf.keras.backend.clear_session()

    model = Sequential()
    model.add(Input(input_shape))

    for i in range(nb_layers):
        model.add(LSTM(internal_size,
                       return_sequences = True if i < nb_layers -1 else False))
    model.add(Dense(input_shape[-1], activation="softmax"))
    model.compile(loss='sparse_categorical_crossentropy', optimizer="adam")

    print(model.summary())

    return model

def generate_gru_model(input_shape, internal_size = 1024, nb_layers = 1):
    """Build a tensorflow GRU model

    Parameters
    ----------
    input_shape : tuple
        The shape of the input vector
    internal_size : int
        The number of units in the GRU layers
    nb_layers : int
        The number of GRU layers

    Returns
    -------
    model : tensorflow.model
    """
    # clear all previous session
    tf.keras.backend.clear_session()

    model = Sequential()
    model.add(Input(input_shape))

    for i in range(nb_layers):
        model.add(GRU(internal_size,
                      return_sequences = True if i < nb_layers -1 else False,
                      recurrent_initializer='glorot_uniform'))
    model.add(Dense(input_shape[-1], activation="softmax"))
    model.compile(loss='sparse_categorical_crossentropy', optimizer="adam")

    print(model.summary())

    return model