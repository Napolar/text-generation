#!/usr/bin/env python
# -*- coding: utf-8 -*-
r""" Recover a LSTM trained model for generating text

    eg : `python run_lstm.py`
    will generate 10 predictions of 200 letters.
"""

import os
import argparse

from tensorflow.keras.models import load_model
from utils.data_processing import download_dataset, prepare_dataset
from utils.generators import generate_text
from utils.models import generate_lstm_model

LSTM_WEIGHTS_URL = "./weights/lstm.hdf5"

# force CPU (untested on GPU)
os.environ['CUDA_VISIBLE_DEVICES'] = '-1'

parser = argparse.ArgumentParser()
parser.add_argument("-d", "--diversity",   type=list, help="Diversity score, give more chance to the unpredicted letters", default=[0.01, 0.5, 0.9])
parser.add_argument("-l", "--pred_len",    type=int, help="The length of a prediction (in letters)", default=200)
parser.add_argument("-n", "--pred_number", type=int, help="The number of predictions", default=10)

args = parser.parse_args()

if __name__ == "__main__":
    # get the dataset, and encode each chars
    dataset = download_dataset()
    X, Y, uniques_chars, char_to_indice, indice_to_char = \
                        prepare_dataset(dataset)

    print("Loading weights...")
    model = load_model(LSTM_WEIGHTS_URL)

    print("Start generating")
    for i in range(args.pred_number):
        generate_text(model,
                      dataset,
                      uniques_chars,
                      char_to_indice,
                      indice_to_char,
                      X.shape[1],
                      diversities=args.diversity,
                      prediction_len=args.pred_len)

