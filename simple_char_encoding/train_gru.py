#!/usr/bin/env python
# -*- coding: utf-8 -*-
r""" Generate text based on Baudelaire 'Les Fleurs du Mal' using GRU

There are several machine learning models that allow us to generate text,
using an architecture based on GRU, and a public dataset, we prepare the
data using character encoding, train a model, and generate text.

After training the model will be saved into the weights directory.

"""
import os
import datetime

from utils.data_processing import download_dataset, prepare_dataset
from utils.generators import generate_writer_callback
from utils.models import generate_gru_model

# force CPU (untested on GPU)
os.environ['CUDA_VISIBLE_DEVICES'] = '-1'

if __name__ == "__main__":
    dataset = download_dataset()

    X, Y, uniques_chars, char_to_indice, indice_to_char =\
                        prepare_dataset(dataset)

    input_shape = X.shape[1:]
    model = generate_gru_model(input_shape)

    callbacks = [generate_writer_callback(model,
                                          dataset,
                                          uniques_chars,
                                          char_to_indice,
                                          indice_to_char,
                                          X.shape[1],
                                          print_every=1,
                                          min_epoch=2)
                ]

    model.fit(X, Y,
              batch_size=32,
              epochs=2000,
              verbose=1,
              callbacks=callbacks)

    today = datetime.date.today()
    year  = today.year
    month = today.month
    day   = today.day

    model.save('./weights/gru_{}_{}_{}'.format(year, month, day))
