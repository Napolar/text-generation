#!/usr/bin/env python
# -*- coding: utf-8 -*-
r""" Collection of callbacks and utilities relative to text generation"""

import sys

import numpy as np
from tensorflow.keras.callbacks import LambdaCallback

__all__ = ('print_green', 'choose_sample', 'generate_text')

def print_green(str):
    """Print the given text in green
    Parameters
    ----------
    str : string
        Text to print in green
    """
    sys.stdout.write("\033[92m {}\033[00m" .format(str))
    sys.stdout.flush()

def choose_sample(predictions, diversity = 1.0, epsilon = 1e-15):
  """Given a predictions (probability array) choose an index
    the diversity factor will give more chance to the unpredicted ones

    Parameters
    ----------
    predictions : array
        The prediction of the model for the next letter
    diversity : float
        Give more chance to the unpredicted letters
    epsilon : float
        To avoid to divide by zero, be carefull as a value to high could cause
        errors or weird behaviour

    Returns
    -------
    char_index : int
        The index wich has be choosen given the multinomial law
  """
  predictions = np.array(predictions).astype('float64')
  predictions = np.log(predictions  + epsilon) / diversity
  predictions = np.exp(predictions)

  sum_exp       = np.sum(predictions)
  probabilities = predictions / ( sum_exp + epsilon )

  # correct the proba to sum to 1 (python floating error)
  sigma = 1.0 - np.sum(probabilities)
  probabilities[np.argmax(probabilities)] += sigma

  # use multinomial to affect to every character a random score based
  # on the predictions of the model
  scores = np.random.multinomial(1, probabilities, 1)

  # return the index who has the better score : the index of the
  # choosen character
  return np.argmax(scores)

def generate_text(model,
                  dataset,
                  w2v,
                  sequence_len,
                  diversity = 0.1,
                  prediction_len = 20,
                  separator = "#"):
    """Generate text using the model

    Parameters
    ----------
    dataset : string
        The string dataset containing the text
    w2v : Word2Vec Model
        Gensim Word2Vec trained model
    sequence_len : int
        The length of the sequence for the model
    print_every : int
        Trigger the generation every print_every epochs
    min_epoch : int
        Threshold to start triggering callback
    diversity : float
        The diversity value to try at every generation
    prediction_len : int
        The number of word generated
    separator : string
        The char to separe when priting result

    Returns
    -------
    gen_callback : tensorflow.keras.LambdaCallback
        Tensorflow callback to be called
    """
    # generate a static function and wrap it as a callback
    def callback(epoch_number, _):
        print('\n'*2)
        print("\n" * 2, separator * 35)
        print(" Generating text for epoch {} ".format(epoch_number))

        # take random sequence
        random_start_index = np.random.randint(0, len(dataset) - sequence_len - 1)

        random_sequence         = dataset[random_start_index : random_start_index + sequence_len]
        random_sequence_encoded = np.array(list(map(lambda word : w2v.wv[word], random_sequence)))

        generated = " ".join(random_sequence)
        print_green(generated)

        for _ in range(prediction_len):

            next_word_encoded = model.predict(np.expand_dims(random_sequence_encoded, axis=0))[0]

            most_similars   = w2v.most_similar(positive=[next_word_encoded], topn=7)
            index_to_choose = choose_sample([proba for _, proba in most_similars], diversity=diversity)

            next_word         = most_similars[index_to_choose][0]
            next_word_encoded = w2v.wv[next_word]

            # shift the generation with the new word
            random_sequence_encoded = np.concatenate((random_sequence_encoded[1:], np.expand_dims(next_word_encoded, axis=0)))

            # remove the first word
            generated = " ".join((generated).split(" ")[1:])
            # add the new word at the end
            generated += " " + next_word

            sys.stdout.write(" " + next_word)
            sys.stdout.flush()
        print("\n")

    return LambdaCallback(on_epoch_end=callback)