#!/usr/bin/env python
# -*- coding: utf-8 -*-
r""" Utilities relative to the dataset """

import io
import numpy as np
import gensim
from gensim.models import Word2Vec


__all__ = ('download_dataset', 'prepare_dataset')

BAUDELAIRE_DATASET_URL = "../dataset/baudelaire.txt"

# to prepare all the possible sequence of text given a fixed size we need to
# define this fixed size, and also the step beetwen two sequences
SEQUENCE_LEN   = 20
SEQUENCE_DELTA = 5

def download_dataset(url = BAUDELAIRE_DATASET_URL):
    """Download the given dataset and return it as a string (lowercase)

    Parameters
    ----------
    url : str
        The url link of the dataset

    Raises
    ------
    FileNotFound
        If the dataset is empty

    Returns
    -------
    dataset : str
        The dataset as lowercase string
    """
    dataset = ""
    with io.open(url, encoding='utf-8') as f:
        dataset = f.read().lower()

    # sanity check
    if len(dataset) < 1:
        raise FileNotFoundError()


    return dataset

def prepare_dataset(dataset, sequence_len = SEQUENCE_LEN,
                     sequence_delta = SEQUENCE_DELTA):
    """Prepare the dataset using word2vec encoding

    We will generate all the possible sequences of text
    limited by a number of char : X. Then the
    Y will be the letter following this sequence.

    Parameters
    ----------
    dataset : str
        The text dataset as string
    sequence_len : int
        The length of the each sequence
    sequence_delta : int
        The number of letter to skip beetwen two sequences

    Returns
    -------
    X : array of array of chars
        All the possible sequence of text to train
    Y : array of chars
        The missing letter for each sequence
    w2v : Word2Vec Model
        The gensim Word2Vec trained model
    dataset : array of str
        Formatted dataset, word by word
    """
    print("\n\nThe corpus contains {} letters".format(len(dataset)))

    # Prepare the sentences one by one, and add an '\n' at every end
    sentences = list(map(lambda sentence : gensim.utils.simple_preprocess(sentence), dataset.split('\n')))
    for sentence in sentences:
        sentence.append('\n')

    # train the Word2Vec model
    w2v = Word2Vec(sentences=sentences, size=75, window=3, min_count=1)

    # display some similarities
    for w in ["langueur", "papillons", "ennuyés"]:
        similars = w2v.wv.most_similar(positive=[w], topn=3)
        print("Words similar to '{}'".format(w))
        for w, score in similars:
            if len(w) < 4:
                pass
            else:
                print("   Word \'{}\' with score {}".format(w, round(score, 2)))

    # encode the dataset as a list word by word ('\n' considered as word)
    word_by_word = []

    for sentence in sentences:
        for word in sentence:
            word_by_word.append(word)

    word_by_word = np.array(word_by_word)

    """
    Data preparation
    open the dataset, and prepare the data, for each sentence
    store the next word
    """
    sentences  = []
    next_words = []

    X = []
    Y = []

    # retrieve all the possible sequences, and for each of them
    # the word after, this will be the word to predict
    sentences  = []
    next_chars = []
    for i in range(0, len(word_by_word) - sequence_len, sequence_delta):
        sentence       = word_by_word[i:i + sequence_len]
        sentence_encoded = list(map(lambda word : w2v.wv[word], sentence))

        next_word         = word_by_word[i + sequence_len]
        next_word_encoded = w2v.wv[next_word]

        sentences.append(sentence)
        next_words.append(next_word)
        X.append(sentence_encoded)
        Y.append(next_word_encoded)

    sentences = np.array(sentences)
    next_word = np.array(next_word)

    X = np.array(X, dtype='float32')
    Y = np.array(Y, dtype="float32")

    # We now have X, Y ready for the training process
    print("We have {} samples for training, shape of X : {}".format(len(sentences), X.shape))
    print("Data preparation completed.")

    return X, Y, w2v, word_by_word