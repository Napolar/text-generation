#!/usr/bin/env python
# -*- coding: utf-8 -*-
r""" Generate text based on Baudelaire 'Les Fleurs du Mal' using LSTM

There are several machine learning models that allow us to generate text,
using an architecture based on LSTM, and a public dataset, we prepare the
data using character encoding, train a model, and generate text.

After training the model will be saved into the weights directory.

"""
import os
import datetime

from utils.data_processing import download_dataset, \
                                  prepare_dataset, SEQUENCE_LEN
from utils.generators import generate_text
from utils.models import generate_lstm_model

# force CPU (untested on GPU)
os.environ['CUDA_VISIBLE_DEVICES'] = '-1'

if __name__ == "__main__":
    dataset = download_dataset()

    X, Y, w2v, word_by_word = prepare_dataset(dataset, SEQUENCE_LEN)

    input_shape = X.shape[1:]
    model = generate_lstm_model(input_shape)

    callbacks = [generate_text(model, word_by_word, w2v, SEQUENCE_LEN)]

    model.fit(X, Y,
              batch_size=32,
              epochs=2000,
              verbose=1,
              callbacks=callbacks)

    today = datetime.date.today()
    year  = today.year
    month = today.month
    day   = today.day

    model.save('./weights/w2v_lstm_{}_{}_{}'.format(year, month, day))
